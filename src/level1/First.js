import { useState, useEffect } from "react"
import "../App.css"

function First({ shopId }) {
  const [value, setValue] = useState(0)
  const [toSearch, setToSearch] = useState(0)
  const [result, setResult] = useState(null)

  useEffect(() => {
    onValidate()
  }, [toSearch])

  const choseValue = (value) => {
    setResult(null)
    setValue(value)
    setToSearch(value)
  }

  const onValidate = async () => {
    const rep = await fetch(`http://localhost:3000/shop/${shopId}/search-combination?amount=${toSearch}`, {
      headers: {
        Authorization: "tokenTest123",
      },
    }).then((r) => r.json())
    if (!rep.equal && !rep.floor && rep.ceil.value > toSearch) return choseValue(rep.ceil.value)
    if (!rep.equal && !rep.ceil && rep.floor.value < toSearch) return choseValue(rep.floor.value)
    return setResult(rep)
  }

  return (
    <div className="container">
      <h1>Level 1</h1>
      <div className="col maxW">
        <input type="number" value={value} onChange={(e) => setValue(e.target.value)} className="margin-bottom input" />
        <button className="btn" onClick={() => setToSearch(value)}>
          Valider
        </button>
      </div>
      {result && !result.equal && (
        <div>
          <h2>Les choix possible les plus proches :</h2>
          {result.ceil && (
            <button className="btn" onClick={() => choseValue(result.ceil.value)}>
              {result.ceil.value}
            </button>
          )}
          {result.floor && (
            <button className="btn" onClick={() => choseValue(result.floor.value)}>
              {result.floor.value}
            </button>
          )}
        </div>
      )}
      {result && result.equal && (
        <div>
          <h2>Votre montant est composé des cartes suivantes :</h2>
          {result.equal.cards.map((card, id) => (
            <p key={id}>- {card} €</p>
          ))}
        </div>
      )}
    </div>
  )
}

export default First
