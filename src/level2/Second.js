import { useState } from "react"
import "../App.css"

function First({ shopId }) {
  const [value, setValue] = useState(0)
  const [result, setResult] = useState(null)

  const doRequest = (value) => {
    return fetch(`http://localhost:3000/shop/${shopId}/search-combination?amount=${value}`, {
      headers: {
        Authorization: "tokenTest123",
      },
    }).then((r) => r.json())
  }

  const askNextValue = (nextValue) => {
    setValue(nextValue)
    onValidate(nextValue)
  }

  const onValidate = async (value) => {
    const equal = await doRequest(value)
    var ceil = undefined
    var floor = undefined
    if (equal.equal) {
      if (equal.ceil) ceil = await doRequest(value + 1)
      if (equal.floor) floor = await doRequest(value - 1)
      if (ceil.ceil) equal.ceil = ceil.ceil
      equal.floor = floor.floor?.value === value ? undefined : floor.floor
      equal.ceil = ceil.ceil?.value === value ? undefined : ceil.ceil
    }
    return setResult(equal)
  }

  return (
    <div className="container">
      <h1>Level 2</h1>
      <div className="row">
        <div className="margin-bottom col maxW">
          <input className="margin-bottom input" type="number" value={value} onChange={(e) => setValue(e.target.value)} />
          <button className="btn" onClick={() => onValidate(value)}>
            Valider
          </button>
        </div>
        {result && (
          <div className="col ">
            <button disabled={!result.floor} className="btn" onClick={() => askNextValue(result.floor.value)}>
              Montant précedent
            </button>
            <button className="btn" disabled={!result.ceil} onClick={() => askNextValue(result.ceil.value)}>
              Montant suivant
            </button>
          </div>
        )}
      </div>

      {result && result.equal && (
        <div>
          <h2>Votre montant est composé des cartes suivantes :</h2>
          {result.equal.cards.map((card, id) => (
            <p key={id}>- {card} €</p>
          ))}
        </div>
      )}
    </div>
  )
}

export default First
