import "./App.css"
import First from "./level1/First"
import Second from "./level2/Second"
function App() {
  return (
    <div className="App">
      <First shopId={5} />
      <Second shopId={5} />
    </div>
  )
}

export default App
